package com.pagodalabs.newlywed.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.broadcast.KillActivity;
import com.pagodalabs.newlywed.utils.Utils;

public class AboutUsActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private TextView tvAboutSub;
    private TextView tvAboutDescription;
    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);
        context = this;
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvAboutSub = (TextView) findViewById(R.id.tvAboutSub);
        tvAboutDescription = (TextView) findViewById(R.id.tvAboutDescription);
        tvAboutSub.setText("");
        tvAboutDescription.setText("“It takes real planning to organize this kind of chaos” Mel Odom.\n" +
                "\n" +
                "Before we dream of a perfect man, some of us dream of a perfect wedding day and in order for this perfect wedding to take place, planning becomes an integral component.\n" +
                "\n" +
                "There are insurmountable number of tasks and obstacles one faces when planning a wedding- to name a few, bridal dress, flowers, astrologer, wine, puja saman, beautician, cosmetics, vehicles and the list goes on and on and on. *Sigh*, overwhelming is an understatement which is why we are here to assist you with planning one of the most memorable days in your life. The website consists of list of vendors for everything you need to plan a wedding from beauticians to photo studios to catering services. The main attraction of the website is the wedding planner tool. In order to make this process more accessible for you, the mobile application which you can download if you click here will allow you to receive reminders on appointments and it will definitely make the wedding planning process a tad bit less stressful, we promise J [we can add a tutorial of how to use this app?]\n" +
                "\n" +
                "The website also includes blogs of others going through the journey of planning a wedding and all the emotions that encompass this journey along with stories on love, relationship, marriage, family and much more. The website also encompasses information that newlyweds should be aware of including sexual and reproductive health related information such as contraceptives, safe abortion and sexual health.");
        broadcastReceiver = KillActivity.killActivity(this);
        KillActivity.registerReceiver(context);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.default_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        Utils.popUpMenu(context, id);


        return super.onOptionsItemSelected(item);
    }


}
