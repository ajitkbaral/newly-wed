package com.pagodalabs.newlywed.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.adapters.InspirationAdapter;
import com.pagodalabs.newlywed.broadcast.KillActivity;
import com.pagodalabs.newlywed.constant.Key;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.entities.Inspiration;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.touchlistener.RecyclerTouchListener;
import com.pagodalabs.newlywed.utils.Utils;
import com.pagodalabs.newlywed.utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class InspirationalActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView tvVolleyError;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private RecyclerView rvInspiration;

    private Context context;

    private InspirationAdapter inspirationAdapter;

    private ArrayList<Inspiration> inspirationArrayList;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspiration);

        context = this;

        toolbar = (Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();

        rvInspiration = (RecyclerView) findViewById(R.id.rvInspiration);
        tvVolleyError = (TextView)findViewById(R.id.tvVolleyError);
        rvInspiration.setLayoutManager(new LinearLayoutManager(context));
        inspirationAdapter = new InspirationAdapter(context);
        rvInspiration.setAdapter(inspirationAdapter);


        sendJsonRequest();

        rvInspiration.addOnItemTouchListener(new RecyclerTouchListener(this, rvInspiration, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View v, int position) {
                Inspiration inspiration = inspirationArrayList.get(position);
                Intent inspirationDetail = new Intent(context, SexplorationDetailActivity.class);
                inspirationDetail.putExtra("inspiration", inspiration);
                startActivity(inspirationDetail);

            }

            @Override
            public void onLongClick(View v, int position) {

            }

        }));
        KillActivity.killActivity(this);
        KillActivity.registerReceiver(context);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.default_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        Utils.popUpMenu(context, id);


        return super.onOptionsItemSelected(item);
    }

    private void sendJsonRequest() {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                URLS.jsonInspiration,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        tvVolleyError.setVisibility(View.GONE);
                        parseJSONResponse(response);
                        inspirationAdapter.setInspirationArrayList(inspirationArrayList);
                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        VolleyErrorMessage.handleVolleyErrors(tvVolleyError, error);
                        progressDialog.dismiss();

                    }
                });
        requestQueue.add(request);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Content....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private ArrayList<Inspiration> parseJSONResponse(JSONObject response) {

        inspirationArrayList = new ArrayList<Inspiration>();
        if (response != null || response.length() > 0){
            try {

                if (response.has("inspirations")) {
                    JSONArray jsonArray = response.getJSONArray("inspirations");

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        int pageId = jsonObject.getInt(Key.pageId);
                        String pageTitle = jsonObject.getString(Key.pageTitle);
                        String description = jsonObject.getString(Key.description);
                        String imageName = jsonObject.getString(Key.imageName);
                        String modifiedDate = jsonObject.getString(Key.modifiedDate);
                        inspirationArrayList.add(new Inspiration(pageId, pageTitle, Utils.html2text(description), URLS.inspirationImageUrl+imageName, modifiedDate));

                    }
                }

            } catch (JSONException jsonException) {

            }
        }
        return inspirationArrayList;
    }

}
