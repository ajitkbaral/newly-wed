package com.pagodalabs.newlywed.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.adapters.MaritalAdapter;
import com.pagodalabs.newlywed.adapters.SexplorationAdapter;
import com.pagodalabs.newlywed.broadcast.KillActivity;
import com.pagodalabs.newlywed.constant.Key;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.entities.Marital;
import com.pagodalabs.newlywed.entities.Sexploration;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.touchlistener.RecyclerTouchListener;
import com.pagodalabs.newlywed.utils.Utils;
import com.pagodalabs.newlywed.utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MaritalActivity extends AppCompatActivity {

    /*private Toolbar toolbar;
    private RecyclerView rvMarital;
    private Context context;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private TextView tvVolleyError;
    private ProgressDialog progressDialog;
    private String maritalFromPref;*/
    private BroadcastReceiver broadcastReceiver;

    private Toolbar toolbar;
    private TextView tvVolleyError;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private RecyclerView rvMarital;

    private ArrayList<Sexploration> sexplorationArrayList;
    private SexplorationAdapter sexplorationAdapter;

    private Sexploration sexploration;
    private Context context;

    private MaritalAdapter maritalAdapter;

    private ArrayList<Marital> maritalArrayList;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marital);

        context = this;

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();


        sexploration = (Sexploration) getIntent().getSerializableExtra("sexploration");


        rvMarital = (RecyclerView) findViewById(R.id.rvMarital);
        tvVolleyError = (TextView) findViewById(R.id.tvVolleyError);
        rvMarital.setLayoutManager(new LinearLayoutManager(context));
        maritalAdapter = new MaritalAdapter(context);
        rvMarital.setAdapter(maritalAdapter);




        sendJsonRequest();


        rvMarital.addOnItemTouchListener(new RecyclerTouchListener(this, rvMarital, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View v, int position) {

                Marital marital = maritalArrayList.get(position);
                    Intent maritalDetail = new Intent(context, MaritalDetailActivity.class);
                    maritalDetail.putExtra("marital", marital);
                    startActivity(maritalDetail);


            }

            @Override
            public void onLongClick(View v, int position) {

            }

        }));


        broadcastReceiver = KillActivity.killActivity(this);
        KillActivity.registerReceiver(context);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.default_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        Utils.popUpMenu(context, id);

        return super.onOptionsItemSelected(item);
    }

    /*private List<Marital> jsonForMarital(JSONObject response){
        List<Marital> maritalList = new ArrayList<>();
        if(response == null || response.length()==0){
            return maritalList;
        }else{
            if(response.has(Key.maritals)){
                try {
                    JSONArray maritalsArray = response.getJSONArray(Key.maritals);
                    for(int i = 0; i<maritalsArray.length(); i++){
                        JSONObject maritalObject = maritalsArray.getJSONObject(i);
                        int pageId = maritalObject.getInt(Key.pageId);
                        String pageTitle = maritalObject.getString(Key.pageTitle);
                        String description = maritalObject.getString(Key.description);
                        String imageName = maritalObject.getString(Key.imageName);
                        String modifiedDate = maritalObject.getString(Key.modifiedDate);
                        maritalList.add(new Marital(pageId, pageTitle, description, URLS.maritalImageUrl+imageName, modifiedDate));
                    }
                    return maritalList;
                }catch(JSONException ex){

                }
            }
        }
        return maritalList;
    }*/

    /*private void setUp(final List<Marital> maritalList){
        MaritalAdapter maritalAdapter = new MaritalAdapter(context, maritalList);
        maritalAdapter.setMaritalList(maritalList);
        rvMarital.setAdapter(maritalAdapter);
        rvMarital.addOnItemTouchListener(new RecyclerTouchListener(context, rvMarital, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Marital marital = maritalList.get(position);
                Intent maritalDetail = new Intent(context, MaritalDetailActivity.class);
                maritalDetail.putExtra("marital", marital);
                startActivity(maritalDetail);

            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(context, "Long Click" + position, Toast.LENGTH_SHORT).show();

            }
        }));
        rvMarital.setLayoutManager(new LinearLayoutManager(context));
    }*/

    private void sendJsonRequest() {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                URLS.jsonMarital + sexploration.getId() + ".json",
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        tvVolleyError.setVisibility(View.GONE);
                            parseJSONResponseMarital(response);
                            maritalAdapter.setMaritalList(maritalArrayList);

                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        VolleyErrorMessage.handleVolleyErrors(tvVolleyError, error);
                        progressDialog.dismiss();

                    }
                });
        requestQueue.add(request);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Content....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private ArrayList<Marital> parseJSONResponseMarital(JSONObject response) {
        Log.d("newly wed", response.toString());
        maritalArrayList = new ArrayList<Marital>();
        if (response != null || response.length() > 0) {
            try {
                    if (response.has("maritals")) {
                        JSONArray jsonArray = response.getJSONArray("maritals");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject maritalObject = jsonArray.getJSONObject(i);
                            int pageId = maritalObject.getInt(Key.pageId);
                            String pageTitle = maritalObject.getString(Key.pageTitle);
                            String description = maritalObject.getString(Key.description);
                            String imageName = maritalObject.getString(Key.imageName);
                            String modifiedDate = maritalObject.getString(Key.modifiedDate);
                            maritalArrayList.add(new Marital(pageId, pageTitle, Utils.html2text(description), URLS.maritalImageUrl + imageName, modifiedDate, ""));

                        }
                    }


            } catch (JSONException jsonException) {

            }
        }
        return maritalArrayList;
    }

    private ArrayList<Sexploration> parseJSONResponseSexploration(JSONObject response) {

        sexplorationArrayList = new ArrayList<Sexploration>();
        if (response != null || response.length() > 0){
            try {

                if (response.has("categories")) {
                    JSONArray jsonArray = response.getJSONArray("categories");

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject sexplorationObject = jsonArray.getJSONObject(i);
                        int id = sexplorationObject.getInt("id");
                        String name = sexplorationObject.getString("name");
                        int parentId = sexplorationObject.getInt("parent_id");
                        Sexploration sexploration = new Sexploration(id, name, parentId,0);
                        sexploration.toString();
                        sexplorationArrayList.add(new Sexploration(id, name, parentId,0));

                    }
                }

            } catch (JSONException jsonException) {

            }
        }
        return sexplorationArrayList;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        unregisterReceiver(broadcastReceiver);
        Log.d("Newly Wed", "onBackPressed");
    }

}
