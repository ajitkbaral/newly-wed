package com.pagodalabs.newlywed.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.adapters.ProductAdapter;
import com.pagodalabs.newlywed.broadcast.KillActivity;
import com.pagodalabs.newlywed.constant.Key;
import com.pagodalabs.newlywed.constant.SharedPrefKey;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.entities.Product;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.touchlistener.RecyclerTouchListener;
import com.pagodalabs.newlywed.utils.SharedPref;
import com.pagodalabs.newlywed.utils.Utils;
import com.pagodalabs.newlywed.utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProductActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView tvVolleyError;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private RecyclerView rvProduct;

    private Context context;

    private ProductAdapter productAdapter;

    private ArrayList<Product> productArrayList;
    private ProgressDialog progressDialog;
    private BroadcastReceiver broadcastReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        context = this;

        toolbar = (Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();

        rvProduct = (RecyclerView) findViewById(R.id.rvProduct);
        tvVolleyError = (TextView)findViewById(R.id.tvVolleyError);
        rvProduct.setLayoutManager(new GridLayoutManager(context, 2));
        productAdapter = new ProductAdapter(context);
        rvProduct.setAdapter(productAdapter);


        sendJsonRequest();

        rvProduct.addOnItemTouchListener(new RecyclerTouchListener(this, rvProduct, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View v, int position) {
                Product product = productArrayList.get(position);
                Intent productDetail = new Intent(context, ProductDetailActivity.class);
                productDetail.putExtra("product", product);
                startActivityForResult(productDetail, 1);
            }

            @Override
            public void onLongClick(View v, int position) {

            }

        }));

        broadcastReceiver = KillActivity.killActivity(this);
        KillActivity.registerReceiver(context);



    }
    private void sendJsonRequest() {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                URLS.jsonProduct+"?cat_name="+ SharedPref.readFromPreferences(context,
                        SharedPrefKey.fileName, "categoryName", "")+".json",
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        tvVolleyError.setVisibility(View.GONE);
                        parseJSONResponse(response);
                        productAdapter.setProductList(productArrayList);
                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        VolleyErrorMessage.handleVolleyErrors(tvVolleyError, error);
                        progressDialog.dismiss();

                    }
                });
        requestQueue.add(request);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Content....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private ArrayList<Product> parseJSONResponse(JSONObject response) {

        productArrayList = new ArrayList<Product>();
        if (response != null || response.length() > 0){
            try {

                if (response.has("products")) {
                    JSONArray jsonArray = response.getJSONArray("products");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject productsObject = jsonArray.getJSONObject(i);
                        int id = productsObject.getInt(Key.productId);
                        String productName = productsObject.getString(Key.name);
                        String description = productsObject.getString(Key.productDescription);
                        int quantity = productsObject.getInt(Key.productQuantity);
                        String salePrice = productsObject.getString(Key.productSalePrice);
                        String imageUrl = productsObject.getString(Key.productImageUrl);
                        productArrayList.add(new Product(id, productName, description, quantity, salePrice, URLS.productImageUrl + id + "/" + imageUrl));

                    }
                }

            } catch (JSONException jsonException) {

            }
        }
        return productArrayList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.default_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        Utils.popUpMenu(context, id);

        return super.onOptionsItemSelected(item);
    }





}
