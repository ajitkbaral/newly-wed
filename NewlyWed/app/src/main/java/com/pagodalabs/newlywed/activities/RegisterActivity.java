package com.pagodalabs.newlywed.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.constant.Key;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.utils.VolleyErrorMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private Context context;
    private EditText etUsernameOrEmail;
    private EditText etPassword;
    private Button btnRegister;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        context = this;
        toolbar = (Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        etUsernameOrEmail = (EditText) findViewById(R.id.etUsernameOrEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnRegister = (Button) findViewById(R.id.btnRegister);

        volleySingleton = VolleySingleton.getInstance();

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                String usernameOrEmail = etUsernameOrEmail.getText().toString();
                String password = etPassword.getText().toString();

                if (usernameOrEmail.equals("") && password.equals("")) {
                    Toast.makeText(context, "Both fields are empty", Toast.LENGTH_SHORT).show();
                } else if (usernameOrEmail.equals("")) {
                    Toast.makeText(context, "Username/Email is empty", Toast.LENGTH_SHORT).show();
                } else if (password.equals("")) {
                    Toast.makeText(context, "Password is empty", Toast.LENGTH_SHORT).show();

                } else {
                    sendJsonRequest(usernameOrEmail, password);
                }
            }
        });

    }

    private void sendJsonRequest(String usernameOrEmail, String password) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", usernameOrEmail);
        params.put("password", password);
        requestQueue = volleySingleton.getRequestQueue();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLS.registerPost,
                new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response != null || response.length() == 0) {
                        Log.d("NewLy Wed", response.toString());
                        Boolean success = false;

                        if (response.has(Key.success)) {
                            success = (Boolean) response.get(Key.success);
                            Log.d("NewLy Wed", "Success " + success);
                            if (success) {
                                    startActivity(new Intent(context, LoginActivity.class));
                                    btnRegister.setClickable(true);
                                    progressDialog.dismiss();
                                    finish();
                            }else{
                                btnRegister.setClickable(true);
                                progressDialog.dismiss();
                                Toast.makeText(context, "Something went wrong",
                                        Toast.LENGTH_LONG).show();

                            }
                        }

                    }

                } catch (JSONException jsonException) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Toast.makeText(context, VolleyErrorMessage.handleVolleyErrors(context, error),
                        Toast.LENGTH_LONG).show();
                btnRegister.setClickable(true);


            }
        });

        requestQueue.add(jsonObjectRequest);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Checking Credentials");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

}
