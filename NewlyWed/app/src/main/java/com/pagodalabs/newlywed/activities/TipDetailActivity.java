package com.pagodalabs.newlywed.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.broadcast.KillActivity;
import com.pagodalabs.newlywed.entities.Tip;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.utils.Utils;

public class TipDetailActivity extends AppCompatActivity {
    private Context context;

    private Toolbar toolbar;
    private TextView tvPageTitle;
    private ImageView ivImageName;
    private TextView tvDescription;
    private TextView tvModifiedDate;

    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;
    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tip_detail);
        context = this;
        toolbar = (Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvPageTitle = (TextView)findViewById(R.id.tvPageTitle);
        ivImageName = (ImageView)findViewById(R.id.ivImageName);
        tvDescription = (TextView)findViewById(R.id.tvDescription);
        tvModifiedDate = (TextView)findViewById(R.id.tvModifiedDate);

        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();

        Tip tip = (Tip)getIntent().getSerializableExtra("tip");
        tvPageTitle.setText(tip.getPageTitle());

        String urlThumbnail = tip.getImageName();

        if(urlThumbnail!=null){
            imageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    ivImageName.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    ivImageName.setImageResource(R.drawable.no_image);
                    Toast.makeText(context, "Unable to load image", Toast.LENGTH_LONG).show();
                }
            });
        }

        tvModifiedDate.setText("Date: "+tip.getModifiedDate());

        tvDescription.setText(tip.getDescription().replace("<p>","").replace("</p>",". \n").replace("&rsquo;","'"));

        broadcastReceiver = KillActivity.killActivity(this);
        KillActivity.registerReceiver(context);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.default_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        Utils.popUpMenu(context, id);

        return super.onOptionsItemSelected(item);
    }

}
