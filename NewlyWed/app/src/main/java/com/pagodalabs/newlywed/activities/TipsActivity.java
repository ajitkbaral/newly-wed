package com.pagodalabs.newlywed.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.adapters.TipAdapter;
import com.pagodalabs.newlywed.broadcast.KillActivity;
import com.pagodalabs.newlywed.constant.Key;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.entities.Tip;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.touchlistener.RecyclerTouchListener;
import com.pagodalabs.newlywed.utils.Utils;
import com.pagodalabs.newlywed.utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TipsActivity extends AppCompatActivity {

    /*private Toolbar toolbar;
    private RecyclerView rvTip;
    private Context context;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private TextView tvVolleyError;
    private ProgressDialog progressDialog;
    private String tipFromPref;
    private BroadcastReceiver broadcastReceiver;*/

    private Toolbar toolbar;
    private TextView tvVolleyError;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private RecyclerView rvTip;

    private Context context;

    private TipAdapter tipAdapter;

    private ArrayList<Tip> tipArrayList;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips);

        context = this;

        toolbar = (Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();

        rvTip = (RecyclerView) findViewById(R.id.rvTip);
        tvVolleyError = (TextView)findViewById(R.id.tvVolleyError);
        rvTip.setLayoutManager(new LinearLayoutManager(context));
        tipAdapter = new TipAdapter(context);
        rvTip.setAdapter(tipAdapter);


        sendJsonRequest();

        rvTip.addOnItemTouchListener(new RecyclerTouchListener(this, rvTip, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View v, int position) {
                Tip tip = tipArrayList.get(position);
                Intent tipDetail = new Intent(context, TipDetailActivity.class);
                tipDetail.putExtra("tip", tip);
                startActivity(tipDetail);
            }

            @Override
            public void onLongClick(View v, int position) {

            }

        }));




        KillActivity.killActivity(this);
        KillActivity.registerReceiver(context);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.default_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

            Utils.popUpMenu(context, id);

        return super.onOptionsItemSelected(item);
    }


    /*private List<Tip> jsonForTip(JSONObject response){
        List<Tip> tipList = new ArrayList<>();
        if(response == null || response.length()==0){
            return tipList;
        }else{
            if(response.has(Key.tips)){
                try {
                    JSONArray tipsArray = response.getJSONArray(Key.tips);
                    for(int i = 0; i<tipsArray.length(); i++){
                        JSONObject inspirationObject = tipsArray.getJSONObject(i);
                        int pageId = inspirationObject.getInt(Key.pageId);
                        String pageTitle = inspirationObject.getString(Key.pageTitle);
                        String description = inspirationObject.getString(Key.description);
                        String imageName = inspirationObject.getString(Key.imageName);
                        String modifiedDate = inspirationObject.getString(Key.modifiedDate);
                        tipList.add(new Tip(pageId, pageTitle, description, URLS.tipImageUrl+imageName, modifiedDate));
                    }
                    return tipList;
                }catch(JSONException ex){

                }
            }
        }
        return tipList;
    }


    private void setUp(final List<Tip> tipList){
        TipAdapter tipAdapter = new TipAdapter(context, tipList);
        tipAdapter.setTipList(tipList);
        rvTip.setAdapter(tipAdapter);
        rvTip.addOnItemTouchListener(new RecyclerTouchListener(context, rvTip, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Tip tip = tipList.get(position);
                Intent tipDetail = new Intent(context, TipDetailActivity.class);
                tipDetail.putExtra("tip", tip);
                startActivity(tipDetail);

            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(context, "Long Click" + position, Toast.LENGTH_SHORT).show();

            }
        }));
        rvTip.setLayoutManager(new LinearLayoutManager(context));
    }*/

    private void sendJsonRequest() {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                URLS.jsonTip,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        tvVolleyError.setVisibility(View.GONE);
                        parseJSONResponse(response);
                        tipAdapter.setTipList(tipArrayList);
                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        VolleyErrorMessage.handleVolleyErrors(tvVolleyError, error);
                        progressDialog.dismiss();

                    }
                });
        requestQueue.add(request);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Content....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private ArrayList<Tip> parseJSONResponse(JSONObject response) {

        tipArrayList = new ArrayList<Tip>();
        if (response != null || response.length() > 0){
            try {

                if (response.has("tips")) {
                    JSONArray jsonArray = response.getJSONArray("tips");

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject inspirationObject = jsonArray.getJSONObject(i);
                        int pageId = inspirationObject.getInt(Key.pageId);
                        String pageTitle = inspirationObject.getString(Key.pageTitle);
                        String description = inspirationObject.getString(Key.description);
                        String imageName = inspirationObject.getString(Key.imageName);
                        String modifiedDate = inspirationObject.getString(Key.modifiedDate);
                        tipArrayList.add(new Tip(pageId, pageTitle, Utils.html2text(description), URLS.tipImageUrl+imageName, modifiedDate));

                    }
                }

            } catch (JSONException jsonException) {

            }
        }
        return tipArrayList;
    }


}
