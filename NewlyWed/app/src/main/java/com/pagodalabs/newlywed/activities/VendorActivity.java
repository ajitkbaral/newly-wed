package com.pagodalabs.newlywed.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.adapters.VendorAdapter;
import com.pagodalabs.newlywed.broadcast.KillActivity;
import com.pagodalabs.newlywed.constant.SharedPrefKey;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.entities.Vendor;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.touchlistener.RecyclerTouchListener;
import com.pagodalabs.newlywed.utils.SharedPref;
import com.pagodalabs.newlywed.utils.Utils;
import com.pagodalabs.newlywed.utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VendorActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private Toolbar toolbar;
    private TextView tvVolleyError;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private RecyclerView rvVendor;
    private String vendorTypeName;

    private Context context;

    private VendorAdapter vendorAdapter;

    private ArrayList<Vendor> vendorArrayList;
    private ProgressDialog progressDialog;

    private SwipeRefreshLayout swipeRefreshLayout;

    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor);

        context = this;

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        vendorTypeName = SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "vendorTypeName", "");

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.srLayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();

        rvVendor = (RecyclerView) findViewById(R.id.rvVendor);
        tvVolleyError = (TextView) findViewById(R.id.tvVolleyError);
        rvVendor.setLayoutManager(new GridLayoutManager(context, 3));
        vendorAdapter = new VendorAdapter(context);
        rvVendor.setAdapter(vendorAdapter);


        sendJsonRequest();


        rvVendor.addOnItemTouchListener(new RecyclerTouchListener(this, rvVendor, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View v, int position) {
                Vendor vendor = vendorArrayList.get(position);

                Intent intentVendorDetail = new Intent(context, VendorDetailActivity.class);
                intentVendorDetail.putExtra("vendor", vendor);
                startActivity(intentVendorDetail);

            }

            @Override
            public void onLongClick(View v, int position) {

            }

        }));

        broadcastReceiver = KillActivity.killActivity(this);
        KillActivity.registerReceiver(context);


    }

    private void sendJsonRequest() {
        String url = URLS.jsonVendor+vendorTypeName+".json",
                urlWithoutSpace=url.replace(" ","%20");
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                urlWithoutSpace,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        tvVolleyError.setVisibility(View.GONE);
                        Log.d("Newly Wed", response.toString());
                        parseJSONResponse(response);
                        vendorAdapter.setVendorArrayList(vendorArrayList);
                        if (swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                            Toast.makeText(context, "Latest data updated", Toast.LENGTH_SHORT).show();
                        }
                        progressDialog.dismiss();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (vendorArrayList == null || vendorArrayList.size() == 0) {

                            VolleyErrorMessage.handleVolleyErrors(tvVolleyError, error);

                        }

                        if (swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                            Toast.makeText(context, "Unable to get updates", Toast.LENGTH_SHORT).show();
                        }
                        progressDialog.dismiss();

                    }
                });
        requestQueue.add(request);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Content....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private ArrayList<Vendor> parseJSONResponse(JSONObject response) {

        vendorArrayList = new ArrayList<Vendor>();
        if (response != null || response.length() > 0) {
            try {

                if (response.has("vendors")) {
                    JSONArray arrayVendors = response.getJSONArray("vendors");

                    for (int i = 0; i < arrayVendors.length(); i++) {
                        JSONObject vendorJson = arrayVendors.getJSONObject(i);
                        Vendor vendor = new Vendor();
                        vendor.setVendorId((vendorJson.getInt("id")));
                        vendor.setVendorName(vendorJson.getString("vendor_name"));
                        vendor.setDescription(vendorJson.getString("description"));
                        vendor.setPhone(vendorJson.getString("phone"));
                        vendor.setAddress(vendorJson.getString("address"));
                        vendor.setState(vendorJson.getString("state"));
                        vendor.setCity(vendorJson.getString("city"));
                        vendor.setVendorImage(URLS.vendorImageUrl + vendorJson.getString("vendor_image"));
                        vendorArrayList.add(vendor);

                    }
                }

            } catch (JSONException jsonException) {

            }
        }
        return vendorArrayList;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.default_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        Utils.popUpMenu(context, id);
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        unregisterReceiver(broadcastReceiver);
        Log.d("Newly Wed", "onBackPressed");
    }


    @Override
    public void onRefresh() {
        sendJsonRequest();
    }
}
