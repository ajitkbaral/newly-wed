package com.pagodalabs.newlywed.activities;

import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.constant.SharedPrefKey;
import com.pagodalabs.newlywed.dialogs.AppointmentDialog;
import com.pagodalabs.newlywed.entities.Vendor;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.utils.SharedPref;

public class VendorDetailActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;
    private RequestQueue requestQueue;
    private TextView tvVendorName;
    private TextView tvDescription;
    private TextView tvAddress;
    private ImageView ivVendorImage;
    private Button btnAppointment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_detail);
        context = this;
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        final Vendor vendor = (Vendor)getIntent().getSerializableExtra("vendor");
        tvVendorName = (TextView)findViewById(R.id.tvVendorName);
        tvAddress = (TextView)findViewById(R.id.tvAddress);
        tvDescription = (TextView)findViewById(R.id.tvDescription);
        ivVendorImage = (ImageView)findViewById(R.id.ivVendorImage);
        btnAppointment = (Button)findViewById(R.id.btnAppointment);
        if(SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "login", "").equalsIgnoreCase("")){
            btnAppointment.setVisibility(View.GONE);
        }else{
            btnAppointment.setVisibility(View.VISIBLE);
        }
        btnAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getFragmentManager();
                AppointmentDialog appointmentDialog = new AppointmentDialog();
                appointmentDialog.setVendor(false);
                appointmentDialog.setContext(context);
                appointmentDialog.setVendorId(vendor.getVendorId());
                appointmentDialog.show(manager, "Add Appointment");
            }
        });
        tvVendorName.setText(vendor.getVendorName());
        tvAddress.setText(vendor.getAddress());
        tvDescription.setText(vendor.getDescription());

        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();
        requestQueue = volleySingleton.getRequestQueue();

        String urlThumbnail = vendor.getVendorImage();

        if (urlThumbnail != null) {
            imageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {

                @Override
                public void onResponse(final ImageLoader.ImageContainer response, boolean isImmediate) {
                    ivVendorImage.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    ivVendorImage.setImageResource(R.drawable.no_image);
                }
            });
        }
    }

}
