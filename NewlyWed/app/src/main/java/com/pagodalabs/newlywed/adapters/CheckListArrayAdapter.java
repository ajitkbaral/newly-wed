package com.pagodalabs.newlywed.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.entities.CheckList;

import java.util.List;

/**
 * Created by Ajit Kumar Baral on 1/5/2016.
 */
public class CheckListArrayAdapter extends ArrayAdapter<CheckList> {

    private List<CheckList> checkLists;
    private Context context;
    boolean checkAll_flag = false;
    boolean checkItem_flag = false;

    public CheckListArrayAdapter(Context context, List<CheckList> checkLists) {
        super(context, R.layout.layout_check_list_item, checkLists);
        this.context = context;
        this.checkLists = checkLists;
    }

    static class ViewHolder {
        protected TextView tvParentHeading;
        protected CheckBox cbTitle;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_check_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.tvParentHeading = (TextView) convertView.findViewById(R.id.tvParentHeading);
            viewHolder.cbTitle = (CheckBox) convertView.findViewById(R.id.cbTitle);
            viewHolder.cbTitle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int getPosition = (Integer) buttonView.getTag();  // Here we get the position that we have set for the checkbox using setTag.
                    checkLists.get(getPosition).setSelected(buttonView.isChecked()); // Set the value of checkbox to maintain its state.

                }
            });
            convertView.setTag(viewHolder);
            convertView.setTag(R.id.tvParentHeading, viewHolder.tvParentHeading);
            convertView.setTag(R.id.cbTitle, viewHolder.cbTitle);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.cbTitle.setTag(position); // This line is important.

        if (checkLists.get(position).getParentName().equals("null")) {
            viewHolder.tvParentHeading.setVisibility(View.GONE);
            viewHolder.cbTitle.setVisibility(View.GONE);
            viewHolder.cbTitle.setVisibility(View.GONE);
        } else {
            viewHolder.tvParentHeading.setVisibility(View.VISIBLE);
            viewHolder.cbTitle.setVisibility(View.VISIBLE);
            viewHolder.cbTitle.setVisibility(View.VISIBLE);
        }


        if(position>0 && checkLists.get(position).getParentName().equals(checkLists.get(position-1).getParentName())){
            viewHolder.tvParentHeading.setVisibility(View.GONE);
        }

        viewHolder.tvParentHeading.setText(checkLists.get(position).getParentName());
        viewHolder.cbTitle.setText(checkLists.get(position).getTitle());
        viewHolder.cbTitle.setChecked(checkLists.get(position).getSelected());

        return convertView;
    }
}
