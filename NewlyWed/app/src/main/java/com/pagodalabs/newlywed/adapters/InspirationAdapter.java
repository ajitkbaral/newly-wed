package com.pagodalabs.newlywed.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.entities.Inspiration;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.viewholder.InspirationViewHolder;

import java.util.ArrayList;

/**
 * Created by Ajit Kumar Baral on 8/3/2015.
 */
public class InspirationAdapter extends RecyclerView.Adapter<InspirationViewHolder>{

    private ArrayList<Inspiration> inspirationArrayList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;


    public InspirationAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();
    }

    public void setInspirationArrayList(ArrayList<Inspiration> inspirationArrayList) {
        if (inspirationArrayList != null) {
            this.inspirationArrayList = inspirationArrayList;
            notifyItemRangeChanged(0, inspirationArrayList.size());
        }
    }

    @Override
    public InspirationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.layout_inspiration, parent, false);
        InspirationViewHolder inspirationViewHolder = new InspirationViewHolder(view);
        return inspirationViewHolder;
    }

    @Override
    public void onBindViewHolder(final InspirationViewHolder holder, int position) {
        Inspiration currentInspiration = inspirationArrayList.get(position);
        holder.tvPageTitle.setText(currentInspiration.getPageTitle());
        String urlThumbnail = currentInspiration.getImageName();

        if(urlThumbnail!=null){

            imageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    holder.ivImageName.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.ivImageName.setImageResource(R.drawable.no_image);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return inspirationArrayList.size();
    }

}

