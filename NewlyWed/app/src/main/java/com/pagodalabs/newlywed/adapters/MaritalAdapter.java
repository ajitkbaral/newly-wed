package com.pagodalabs.newlywed.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.entities.Marital;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.viewholder.MaritalViewHolder;

import java.util.ArrayList;

/**
 * Created by Ajit Kumar Baral on 8/6/2015.
 */
public class MaritalAdapter extends RecyclerView.Adapter<MaritalViewHolder> {

    private ArrayList<Marital> maritalArrayList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;


    public MaritalAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();
    }

    public void setMaritalList(ArrayList<Marital> maritalArrayList){
        if (maritalArrayList != null) {
            this.maritalArrayList = maritalArrayList;
            notifyItemRangeChanged(0, maritalArrayList.size());
        }
    }

    @Override
    public MaritalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.layout_marital, parent, false);
        MaritalViewHolder viewHolderMarital = new MaritalViewHolder(view);
        return viewHolderMarital;
    }

    @Override
    public void onBindViewHolder(final MaritalViewHolder holder, int position) {
        Marital currentMarital = maritalArrayList.get(position);
        holder.tvPageTitle.setText(currentMarital.getPageTitle());
        String urlThumbnail = currentMarital.getImageName();

        if(urlThumbnail!=null){

            imageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    holder.ivImageName.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.ivImageName.setImageResource(R.drawable.no_image);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return maritalArrayList.size();
    }
}
