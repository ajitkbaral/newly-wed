package com.pagodalabs.newlywed.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.ImageLoader;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.entities.Sexploration;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.viewholder.SexplorationViewHolder;

import java.util.ArrayList;

/**
 * Created by Ajit Kumar Baral on 12/23/2015.
 */
public class SexplorationAdapter extends RecyclerView.Adapter<SexplorationViewHolder> {
    private ArrayList<Sexploration> sexplorationArrayList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;
    private Context context;

    public SexplorationAdapter(Context context){
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();

    }

    public void setSexplorationList(ArrayList<Sexploration> sexplorationArrayList){
        if (sexplorationArrayList != null) {
            this.sexplorationArrayList = sexplorationArrayList;
            notifyItemRangeChanged(0, sexplorationArrayList.size());
        }
    }
    @Override
    public SexplorationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.layout_sexploration, parent, false);
        SexplorationViewHolder viewHolderSexploration = new SexplorationViewHolder(view);
        return viewHolderSexploration;
    }

    @Override
    public void onBindViewHolder(final SexplorationViewHolder holder, int position) {
        final Sexploration sexploration = sexplorationArrayList.get(position);
        holder.tvName.setText(sexploration.getName());
        /*if(sexploration.getSubSexplorations() == 0){*/
            holder.tvSubCategories.setVisibility(View.GONE);
        /*}else{
            holder.tvSubCategories.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, SubCategoriesActivity.class);
                    intent.putExtra("subCategories", sexploration);
                    context.startActivity(intent);
                }
            });
        }*/

    }

    @Override
    public int getItemCount() {
        return sexplorationArrayList.size();
    }
}
