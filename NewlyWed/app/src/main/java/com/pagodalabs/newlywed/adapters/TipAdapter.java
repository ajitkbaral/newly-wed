package com.pagodalabs.newlywed.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.entities.Tip;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.viewholder.TipViewHolder;

import java.util.ArrayList;

/**
 * Created by Ajit Kumar Baral on 8/6/2015.
 */
public class TipAdapter extends RecyclerView.Adapter<TipViewHolder> {

    private ArrayList<Tip> tipArrayList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;

    public TipAdapter(Context context) {

        layoutInflater = LayoutInflater.from(context);
        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();
    }

    public void setTipList(ArrayList<Tip> tipArrayList){

        if (tipArrayList != null) {
            this.tipArrayList = tipArrayList;
            notifyItemRangeChanged(0, tipArrayList.size());
        }
    }

    @Override
    public TipViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater.inflate(R.layout.layout_tip, parent, false);
        TipViewHolder viewHolderTip = new TipViewHolder(view);
        return viewHolderTip;
    }

    @Override
    public void onBindViewHolder(final TipViewHolder holder, int position) {
        Tip currentTip = tipArrayList.get(position);
        holder.tvPageTitle.setText(currentTip.getPageTitle());
        String urlThumbnail = currentTip.getImageName();

        if(urlThumbnail!=null){

            imageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    holder.ivImageName.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.ivImageName.setImageResource(R.drawable.no_image);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return tipArrayList.size();
    }
}
