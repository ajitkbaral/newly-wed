package com.pagodalabs.newlywed.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.entities.Vendor;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.viewholder.VendorViewHolder;

import java.util.ArrayList;

/**
 * Created by Ajit Kumar Baral on 8/4/2015.
 */
public class VendorAdapter extends RecyclerView.Adapter<VendorViewHolder> {

    private ArrayList<Vendor> vendorArrayList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;

    public VendorAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();
    }

    public void setVendorArrayList(ArrayList<Vendor> vendorArrayList) {
        if (vendorArrayList != null) {
            this.vendorArrayList = vendorArrayList;
            notifyItemRangeChanged(0, vendorArrayList.size());
        }
    }


    @Override
    public VendorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.layout_vendor, parent, false);
        VendorViewHolder viewHolderProfession = new VendorViewHolder(view);
        return viewHolderProfession;
    }

    @Override
    public void onBindViewHolder(final VendorViewHolder holder, int position) {

        Vendor vendor = vendorArrayList.get(position);
        holder.tvVendorName.setText(vendor.getVendorName());
        String urlImage = vendor.getVendorImage();
        if (urlImage != null) {
            imageLoader.get(urlImage, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    holder.ivVendorImage.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return vendorArrayList.size();
    }
}
