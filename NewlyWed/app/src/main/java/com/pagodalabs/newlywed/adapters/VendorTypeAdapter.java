package com.pagodalabs.newlywed.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.entities.VendorType;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.viewholder.VendorTypeViewHolder;

import java.util.ArrayList;

/**
 * Created by Ajit Kumar Baral on 8/4/2015.
 */
public class VendorTypeAdapter extends RecyclerView.Adapter<VendorTypeViewHolder> {

    private ArrayList<VendorType> vendorTypeArrayList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;

    public VendorTypeAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();
    }

    public void setVendorTypeArrayList(ArrayList<VendorType> vendorTypeArrayList) {
        if (vendorTypeArrayList != null) {
            this.vendorTypeArrayList = vendorTypeArrayList;
            notifyItemRangeChanged(0, vendorTypeArrayList.size());
        }
    }


    @Override
    public VendorTypeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.layout_vendor_type, parent, false);
        VendorTypeViewHolder viewHolderProfession = new VendorTypeViewHolder(view);
        return viewHolderProfession;
    }

    @Override
    public void onBindViewHolder(final VendorTypeViewHolder holder, int position) {

        VendorType vendorType = vendorTypeArrayList.get(position);
        holder.tvVendorTypeName.setText(vendorType.getVendorTypeName());
        String urlImage = vendorType.getVendorTypeImage();
        if (urlImage != null) {
            imageLoader.get(urlImage, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    holder.ivVendorTypeImage.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return vendorTypeArrayList.size();
    }
}
