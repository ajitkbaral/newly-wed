package com.pagodalabs.newlywed.application;

import android.app.Application;
import android.content.Context;



/**
 * Created by Ajit Kumar Baral on 8/3/2015.
 */
public class NewlyWedApplication extends Application {
    private static NewlyWedApplication sInstance;


    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    public static NewlyWedApplication getsInstance(){
        return sInstance;
    }

    public static Context getAppContext(){
        return sInstance.getApplicationContext();
    }

}
