package com.pagodalabs.newlywed.constant;

/**
 * Created by Ajit Kumar Baral on 8/3/2015.
 */
public class Key {

    public static final String inspirations = "inpirations",
            pageId = "page_id",
            pageTitle = "page_title",
            description = "description",
            imageName = "image_name",
            tag = "tags",
            modifiedDate = "modified_date";

    public static final String products = "products",
            productId = "id",
            name = "name",
            productDescription = "description",
            productQuantity = "quantity",
            productSalePrice = "sale_price",
            productImageUrl = "images";

    public static final String tips = "tips";
    public static final String maritals = "maritals";
    public static final String wishList = "wishlist";
    public static final String success = "success";
    public static final String userData="user_data";
    public static final String id = "id";
    public static final String username="username";
    public static final int categoryCart = 1;
    public static final int categoryWishList = 2;
    public static final String category="category";
    public static final String categoryImage = category+"_image";
    public static final String accessToken="access_token";
    public static final String user="user";
}
