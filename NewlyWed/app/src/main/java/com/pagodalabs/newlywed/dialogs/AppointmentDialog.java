package com.pagodalabs.newlywed.dialogs;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.constant.Key;
import com.pagodalabs.newlywed.constant.SharedPrefKey;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.entities.Vendor;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Ajit Kumar Baral on 8/28/2015.
 */
public class AppointmentDialog extends DialogFragment {

    private Context context;
    private DatePicker dpTo;
    private DatePicker dpFrom;
    private Boolean vendor;
    private TextView tvVendor;
    private EditText etTitle, etDescription;
    private Spinner spVendor;
    private Button btnAdd;
    private Button btnCancel;
    private int vendorId;
    private ArrayList<Vendor> vendorArrayList;
    private ArrayAdapter vendorListAdapter;

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;

    private String userId, accessToken;

    public AppointmentDialog(){
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    public Context getContext(){
        return context;
    }

    public void setContext(Context context){
        this.context = context;
    }

    public Boolean getVendor(){
        return vendor;
    }

    public void setVendor(Boolean vendor){
        this.vendor = vendor;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle("Add Appointment");
        View view = inflater.inflate(R.layout.layout_dialog_appointment, null);
        tvVendor = (TextView)view.findViewById(R.id.tvVendor);
        etTitle = (EditText)view.findViewById(R.id.etTitle);
        etDescription = (EditText)view.findViewById(R.id.etDescription);
        dpTo = (DatePicker)view.findViewById(R.id.dpTo);
        dpFrom = (DatePicker)view.findViewById(R.id.dpFrom);
        spVendor = (Spinner)view.findViewById(R.id.spVendor);
        btnAdd = (Button)view.findViewById(R.id.btnAdd);
        btnCancel = (Button)view.findViewById(R.id.btnCancel);
        dpTo.setMinDate(System.currentTimeMillis() - 1000);
        dpFrom.setMinDate(System.currentTimeMillis() - 1000);

        if(!getVendor()){
            tvVendor.setVisibility(View.GONE);
            spVendor.setVisibility(View.GONE);

            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int fYear = dpFrom.getYear();
                    int fMonth = dpFrom.getMonth() + 1;
                    int fDay = dpFrom.getDayOfMonth();
                    int tYear = dpTo.getYear();
                    int tMonth = dpTo.getMonth() + 1;
                    int tDay = dpTo.getDayOfMonth();
                    sendAddAppointmentRequest(etTitle.getText().toString(), etDescription.getText().toString(), vendorId, fYear + "/" + fMonth + "/" + fDay, tYear + "/" + tMonth + "/" + tDay);
                }
            });

        }else{
            tvVendor.setVisibility(View.VISIBLE);
            spVendor.setVisibility(View.VISIBLE);
            sendJsonRequest();
        }

        userId = SharedPref.readFromPreferences(getContext(), SharedPrefKey.fileName, "userId", "");
        accessToken = SharedPref.readFromPreferences(getContext(), SharedPrefKey.fileName, "accessToken", "");



        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        return view;
    }

    private void sendJsonRequest() {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                URLS.jsonVendorAppointment,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Newly Wed", response.toString());
                        final HashMap<String, Integer> hashMapVendor = new HashMap<String, Integer>();
                        List<String> vendorNameList = new ArrayList<String>();
                        for(Vendor vendor : parseJSONResponse(response)){
                            hashMapVendor.put(vendor.getVendorName(), vendor.getVendorId());
                            vendorNameList.add(vendor.getVendorName());
                        }
                        vendorListAdapter = new ArrayAdapter<String>(getContext(),
                                R.layout.support_simple_spinner_dropdown_item, vendorNameList);

                        spVendor.setAdapter(vendorListAdapter);

                        spVendor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                final String vendorName = (String) parent.getItemAtPosition(position);
                                final int vendorId = hashMapVendor.get(vendorName);

                                btnAdd.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        int fYear = dpFrom.getYear();
                                        int fMonth = dpFrom.getMonth()+1;
                                        int fDay = dpFrom.getDayOfMonth();
                                        int tYear = dpTo.getYear();
                                        int tMonth = dpTo.getMonth()+1;
                                        int tDay = dpTo.getDayOfMonth();
                                        sendAddAppointmentRequest(etTitle.getText().toString(), etDescription.getText().toString(), vendorId, fYear + "/" + fMonth + "/" + fDay, tYear + "/" + tMonth + "/" + tDay);
                                    }
                                });
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        requestQueue.add(request);
    }

    private ArrayList<Vendor> parseJSONResponse(JSONObject response) {

        vendorArrayList = new ArrayList<Vendor>();
        if (response != null || response.length() > 0) {
            try {

                if (response.has("vendors")) {
                    JSONArray arrayVendors = response.getJSONArray("vendors");

                    for (int i = 0; i < arrayVendors.length(); i++) {
                        JSONObject vendorJson = arrayVendors.getJSONObject(i);
                        Vendor vendor = new Vendor();
                        vendor.setVendorId((vendorJson.getInt("id")));
                        vendor.setVendorName(vendorJson.getString("vendor_name"));
                        vendorArrayList.add(vendor);
                    }
                }

            } catch (JSONException jsonException) {

            }
        }
        return vendorArrayList;
    }

    private void sendAddAppointmentRequest(String title, String description, int vendorId, String from, String to) {

        Log.d("Newly Wed", title + description + vendorId + from + to);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", userId);
        params.put("access_token", accessToken);
        params.put("name", title);
        params.put("description", description);
        params.put("vendor_id", String.valueOf(vendorId));
        params.put("from", from);
        params.put("to", to);
        requestQueue = volleySingleton.getRequestQueue();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                URLS.addAppointment, new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Newly Wed", response.toString());
                        if (response.has(Key.success)) {
                            try {
                                Boolean success = (Boolean) response.get(Key.success);
                                if (success) {
                                    Toast.makeText(context, "New Appointment has been added", Toast.LENGTH_SHORT).show();
                                    getDialog().dismiss();
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        requestQueue.add(request);
    }
}
