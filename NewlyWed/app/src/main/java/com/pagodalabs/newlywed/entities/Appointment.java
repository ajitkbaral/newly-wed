package com.pagodalabs.newlywed.entities;

import java.util.Date;

/**
 * Created by Ajit Kumar Baral on 8/28/2015.
 */
public class Appointment {

    private int id;
    private String title;
    private String description;
    private int guests;
    private String venueName;
    private String vendorName;
    private Date from;
    private Date to;
    private String status;

    public Appointment() {
    }

    public Appointment(int id, String title, String description, int guests, String venueName,
                       String vendorName, Date from, Date to, String status) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.guests = guests;
        this.venueName = venueName;
        this.vendorName = vendorName;
        this.from = from;
        this.to = to;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getGuests() {
        return guests;
    }

    public void setGuests(int guests) {
        this.guests = guests;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", guests=" + guests +
                ", venueName='" + venueName + '\'' +
                ", vendorName='" + vendorName + '\'' +
                ", from=" + from +
                ", to=" + to +
                ", status='" + status + '\'' +
                '}';
    }
}
