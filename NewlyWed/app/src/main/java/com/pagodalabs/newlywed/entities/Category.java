package com.pagodalabs.newlywed.entities;

import java.io.Serializable;

/**
 * Created by Ajit Kumar Baral on 8/9/2015.
 */
public class Category implements Serializable{

    private int id;
    private String name;
    private String categoryImage;

    public Category() {
    }

    public Category(int id, String name, String categoryImage) {
        this.id = id;
        this.name = name;
        this.categoryImage = categoryImage;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", categoryImage='" + categoryImage + '\'' +
                '}';
    }
}
