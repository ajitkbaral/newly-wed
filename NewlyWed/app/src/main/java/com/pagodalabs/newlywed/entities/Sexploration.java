package com.pagodalabs.newlywed.entities;

import java.io.Serializable;

/**
 * Created by Ajit Kumar Baral on 12/23/2015.
 */
public class Sexploration implements Serializable {
    private int id;
    private String name;
    private int parentId;
    private int subSexplorations;

    public Sexploration() {
    }

    public Sexploration(int id, String name, int parentId, int subSexplorations) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
        this.subSexplorations = subSexplorations;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getSubSexplorations() {
        return subSexplorations;
    }

    public void setSubSexplorations(int subSexplorations) {
        this.subSexplorations = subSexplorations;
    }

    @Override
    public String toString() {
        return "Sexploration{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", parentId=" + parentId +
                ", subSexplorations=" + subSexplorations +
                '}';
    }
}
