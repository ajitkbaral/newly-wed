package com.pagodalabs.newlywed.entities;

import java.io.Serializable;

/**
 * Created by Ajit Kumar Baral on 1/3/2016.
 */
public class VendorType implements Serializable {

    private int vendorTypeId;
    private String vendorTypeName;
    private String vendorTypeImage;

    public VendorType() {
    }

    public VendorType(int vendorTypeId, String vendorTypeName, String vendorTypeImage) {
        this.vendorTypeId = vendorTypeId;
        this.vendorTypeName = vendorTypeName;
        this.vendorTypeImage = vendorTypeImage;
    }

    public int getVendorTypeId() {
        return vendorTypeId;
    }

    public void setVendorTypeId(int vendorTypeId) {
        this.vendorTypeId = vendorTypeId;
    }

    public String getVendorTypeName() {
        return vendorTypeName;
    }

    public void setVendorTypeName(String vendorTypeName) {
        this.vendorTypeName = vendorTypeName;
    }

    public String getVendorTypeImage() {
        return vendorTypeImage;
    }

    public void setVendorTypeImage(String vendorTypeImage) {
        this.vendorTypeImage = vendorTypeImage;
    }

    @Override
    public String toString() {
        return "VendorType{" +
                "vendorTypeId=" + vendorTypeId +
                ", vendorTypeName='" + vendorTypeName + '\'' +
                ", vendorTypeImage='" + vendorTypeImage + '\'' +
                '}';
    }
}
