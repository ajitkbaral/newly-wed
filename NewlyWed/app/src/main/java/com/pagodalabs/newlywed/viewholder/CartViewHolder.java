package com.pagodalabs.newlywed.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pagodalabs.newlywed.R;

/**
 * Created by Ajit Kumar Baral on 8/7/2015.
 */
public class CartViewHolder extends RecyclerView.ViewHolder {

    public TextView tvSerialNumber;
    public TextView tvProductName;
    public ImageView ivProductImage;
    public TextView tvQuantity;
    public TextView tvPrice;


    public CartViewHolder(View itemView) {
        super(itemView);
        tvSerialNumber = (TextView)itemView.findViewById(R.id.tvSerialNumber);
        tvProductName = (TextView)itemView.findViewById(R.id.tvProductName);
        ivProductImage = (ImageView)itemView.findViewById(R.id.ivProductImage);
        tvQuantity = (TextView)itemView.findViewById(R.id.tvQuantity);
        tvPrice = (TextView)itemView.findViewById(R.id.tvPrice);

    }
}
