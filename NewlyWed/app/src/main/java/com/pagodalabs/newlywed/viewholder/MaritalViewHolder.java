package com.pagodalabs.newlywed.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pagodalabs.newlywed.R;

/**
 * Created by Ajit Kumar Baral on 8/6/2015.
 */
public class MaritalViewHolder extends RecyclerView.ViewHolder {
    public TextView tvPageTitle;
    public ImageView ivImageName;

    public MaritalViewHolder(View itemView) {
        super(itemView);

        tvPageTitle = (TextView)itemView.findViewById(R.id.tvPageTitle);
        ivImageName = (ImageView)itemView.findViewById(R.id.ivImageName);
    }
}
