package com.pagodalabs.newlywed.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pagodalabs.newlywed.R;

/**
 * Created by Ajit Kumar Baral on 8/5/2015.
 */
public class ProductViewHolder extends RecyclerView.ViewHolder {

    public ImageView ivProductImage;
    public TextView tvProductName;
    public TextView tvProductSalePrice;

    public ProductViewHolder(View itemView) {
        super(itemView);

        ivProductImage = (ImageView)itemView.findViewById(R.id.ivProductImage);
        tvProductName = (TextView)itemView.findViewById(R.id.tvProductName);
        tvProductSalePrice = (TextView)itemView.findViewById(R.id.tvProductSalePrice);
    }
}
