package com.pagodalabs.newlywed.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pagodalabs.newlywed.R;

/**
 * Created by Ajit Kumar Baral on 8/4/2015.
 */
public class VendorTypeViewHolder extends RecyclerView.ViewHolder {

    public ImageView ivVendorTypeImage;
    public TextView tvVendorTypeName;

    public VendorTypeViewHolder(View itemView) {
        super(itemView);
        ivVendorTypeImage = (ImageView)itemView.findViewById(R.id.ivVendorTypeImage);
        tvVendorTypeName = (TextView)itemView.findViewById(R.id.tvVendorTypeName);

    }
}
