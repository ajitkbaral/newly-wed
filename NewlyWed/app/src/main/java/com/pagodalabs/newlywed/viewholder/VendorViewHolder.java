package com.pagodalabs.newlywed.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pagodalabs.newlywed.R;

/**
 * Created by Ajit Kumar Baral on 8/4/2015.
 */
public class VendorViewHolder extends RecyclerView.ViewHolder {

    public ImageView ivVendorImage;
    public TextView tvVendorName;

    public VendorViewHolder(View itemView) {
        super(itemView);
        ivVendorImage = (ImageView)itemView.findViewById(R.id.ivVendorImage);
        tvVendorName = (TextView)itemView.findViewById(R.id.tvVendorName);

    }
}
